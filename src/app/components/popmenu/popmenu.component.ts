import {Component, OnInit} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Component({
  selector: 'popmenu',
  templateUrl: './popmenu.component.html',
  styleUrls: ['./popmenu.component.scss']
})
export class PopmenuComponent implements OnInit {

  openMenu: Boolean = false;

  constructor (
      public toastCtrl: ToastController,
  ) {
  }

  ngOnInit() {
  }
}
