import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ToastController, AlertController } from '@ionic/angular';

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authenticationState = new BehaviorSubject(false);

  constructor(
      private storage: Storage,
      private http: HttpClient,
      public alertCtrl: AlertController,
      public toastCtrl: ToastController,
      private plt: Platform) {

    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  login(credentials: any) {
    return this.http.post('http://cs2m.oneway.agency/api/login', credentials)
        .toPromise()
        .then(data => {
          this.loginSuccess();
          this.storage.set('user', data['user']);
          this.storage.set(TOKEN_KEY, data['token']).then(() => {
            this.authenticationState.next(true);
          });
        })
        .catch(e => this.forgotPass(e));
  }

  logout() {
    return this.storage.clear().then(() => {
      this.authenticationState.next(false);
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  async forgotPass(e) {
    const toast = await this.toastCtrl.create({
      message: 'Login ou mot de passe incorrects',
      duration: 3000,
      position: 'top',
      color: 'danger',
      closeButtonText: 'OK',
      showCloseButton: true
    });

    await toast.present();
  }

  async loginSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'Login avec succes',
      duration: 3000,
      position: 'top',
      color: 'success',
      closeButtonText: 'OK',
      showCloseButton: true
    });

    await toast.present();
  }
}
