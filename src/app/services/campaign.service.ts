import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  apiUrl = 'http://cs2m.oneway.agency/api';

  constructor(
      public http: HttpClient,
      public toastCtrl: ToastController,
      private authenticationService: AuthenticationService,
  ) {}

  getCampaign(pin) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/campaign/' + pin).subscribe(data => {
        this.successPopup('Campagne sélectionnée avec succès.');
        resolve(data);
      }, err => {
        if (401 == err.status) { this.authenticationService.logout(); }
        // console.log(err);
        this.errorPopup('Code PIN Invalide.');
      });
    });
  }

  getQuestions(campaign_id) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/campaign/questions/' + campaign_id).subscribe(data => {
        resolve(data);
      }, err => {
        if (401 == err.status) { this.authenticationService.logout(); }
        // console.log(err);
        this.errorPopup('Erreur lors de récupération des questions, Veuillez réessayer. Si le problème persiste merci de contacter l\'admin!');
      });
    });
  }

  sendResponses(responses: []) {
    this.http.post(this.apiUrl + '/campaign/responses/send', responses).subscribe((data) => {
      this.successPopup('Réponses envoyées avec succès.');
    }, err => {
      if (401 == err.status) { this.authenticationService.logout(); }
      // console.log(err);
      this.errorPopup('Erreur lors d\'envoi des réponses, Veuillez réessayer. Si le problème persiste merci de contacter l\'admin!');
    });
  }

  sendRoute(route) {
    return new Promise(resolve => {
      this.http.post(this.apiUrl + '/campaign/position/send', route)
        .subscribe(data => {
          resolve(data);
        }, err => {
          if (401 == err.status) { this.authenticationService.logout(); }
          // console.log(err);
          this.errorPopup('Erreur lors d\'envoi des données GPS. Merci d\'activez l\'accès aux données de localisation de votre appareil. Si le problème persiste merci de contacter l\'admin!');
        });
    });
  }

  sendPicture(picture) {
    return new Promise(resolve => {
      this.http.post(this.apiUrl + '/campaign/picture/send', picture)
        .subscribe(data => {
          resolve(data);
          this.successPopup('Photo envoyées avec succès.');
        }, err => {
          if (401 == err.status) { this.authenticationService.logout(); }
          this.errorPopup('Erreur lors d\'envoi de la photo, Veuillez réessayer. Si le problème persiste merci de contacter l\'admin!');
        });
    });
  }

  async errorPopup(error: string) {
    const toast = await this.toastCtrl.create({
      message: error,
      duration: 5000,
      position: 'top',
      closeButtonText: 'OK',
      color: 'danger',
      showCloseButton: true
    });

    await toast.present();
  }

  async successPopup(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top',
      closeButtonText: 'OK',
      color: 'success',
      showCloseButton: true
    });

    await toast.present();
  }
}
