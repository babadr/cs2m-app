import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { Platform } from '@ionic/angular';

export interface Images {
  id: number;
  campaign_id: number;
  image: string;
  latitude: string;
  longitude: string;
  created_at: string;
  submit_status: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private database: SQLiteObject;
  images = new BehaviorSubject([]);

  constructor(private platform: Platform, private sqlite: SQLite) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
        this.database = db;
        this.createImageTable();
        this.loadImages();
      })
      .catch(e => console.log(e));
    });
  }

  createImageTable() {
    const sql = 'CREATE TABLE IF NOT EXISTS images(id INTEGER PRIMARY KEY AUTOINCREMENT,campaign_id INTEGER,image TEXT,latitude TEXT,longitude TEXT,created_at DATETIME,submit_status BOOLEAN)';

    this.database.executeSql(sql, [])
      // .then(() => console.log('Executed SQL'))
      .catch(e => console.log(e));
  }

  deleteImageTableContent() {
    const sql = 'DELETE FROM images';
    this.database.executeSql(sql, [])
      .then(() => this.loadImages() )
      .catch(e => console.log(e));
  }

  getImages(): Observable<Images[]> {
    return this.images.asObservable();
  }

  loadImages() {
    const sql = 'SELECT * FROM images WHERE created_at >= DATE(\'now\') ORDER BY id DESC';

    return this.database.executeSql( sql, []).then(data => {
      const images: Images[] = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
          images.push({
            id: data.rows.item(i).id,
            campaign_id: data.rows.item(i).campaign_id,
            image: data.rows.item(i).image,
            latitude: data.rows.item(i).latitude,
            longitude: data.rows.item(i).longitude,
            created_at: data.rows.item(i).created_at,
            submit_status: data.rows.item(i).submit_status
          });
        }
      }
      this.images.next(images);
    });
  }

  addImage(imageData) {
    const sql = 'INSERT INTO images(campaign_id, image, latitude, longitude, created_at, submit_status) VALUES (?, ?, ?, ?, ?, ?)';
    return this.database.executeSql(sql, imageData).then(() => {
      this.loadImages();
    });
  }

  updateImageStatus(image_id) {
    const sql = 'UPDATE images SET submit_status = 1 WHERE id = ?';
    return this.database.executeSql(sql, [image_id]).then(() => {
      this.loadImages();
    });
  }

  updateInsertedImageStatus() {
    return this.database.executeSql('SELECT max(id) AS id FROM images', []).then(data => {
      if (data.rows.length > 0) {
        const image_id = data.rows.item(0).id;
        this.updateImageStatus(image_id);
      }
    });
  }

  getImage(id): Promise<Images> {
    const sql = 'SELECT * FROM images WHERE id = ?';
    return this.database.executeSql(sql, [id]).then(data => {
      return {
        id: data.rows.item(0).id,
        campaign_id: data.rows.item(0).campaign_id,
        image: data.rows.item(0).image,
        latitude: data.rows.item(0).latitude,
        longitude: data.rows.item(0).longitude,
        created_at: data.rows.item(0).created_at,
        submit_status: data.rows.item(0).submit_status
      };
    });
  }
}

