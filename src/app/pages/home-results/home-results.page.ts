import { Component } from '@angular/core';
import { NavController, AlertController, MenuController, ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { CampaignService } from '../../services/campaign.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Subscription } from 'rxjs/Subscription';
import { Network } from '@ionic-native/network/ngx';

import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
import { DatabaseService } from '../../services/database.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})

export class HomeResultsPage {
  campaign = {
      id: null,
      pin: null,
      title: null,
      image: 'assets/img/icon.png',
      description: null
  };

  isTracking = false;
  isSupervisor = false;
  trackedRoute = [];
  positionSubscription: Subscription;
  latitude: any;
  longitude: any;

  constructor (
    private storage: Storage,
    private campaignService: CampaignService,
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private geolocation: Geolocation,
    private camera: Camera,
    private database: DatabaseService,
    private toastCtrl: ToastController,
    private loading: LoadingService,
    private network: Network
  ) {
    const disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.navCtrl.navigateRoot('/disconnected');
      disconnectSubscription.unsubscribe();
    });

    this.setCampaign();
    this.checkSupervisor();
  }

  async setCampaign() {
    this.storage.get('campaign').then((campaign) => {
      if (campaign) {
        this.campaign.id = campaign.id;
        this.campaign.pin = campaign.pin;
        this.campaign.title = campaign.title;
        this.campaign.description = campaign.description;
        this.campaign.image = 'http://cs2m.oneway.agency/img/cache/resize/' + campaign.visual;
      }
    });

    this.storage.get('trackingStatus').then((trackingStatus) => {
      if (trackingStatus == true) {
        this.startTracking();
      }
    });
  }

  async checkSupervisor() {
    this.storage.get('user').then((user) => {
      if (user) {
        this.isSupervisor = user.isSupervisor === true;
      }
    });
  }

  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'PIN de la campagne',
      message: 'Insérer le code PIN de la campagne',
      inputs: [
        {
          name: 'pin',
          placeholder: 'Insérez votre code PIN',
          type: 'number'
        },
      ],
      buttons: [
        {
          text: 'Envoyer',
          handler: async (data) => {
            this.campaignService.getCampaign(data.pin)
            .then((campaign) => {
              this.storage.set('campaign', campaign).then(() => {
                this.setCampaign();
              });
            });
          }
        }
      ]
    });

    changeLocation.present();
  }

  async startTracking() {
    this.isTracking = true;
    this.storage.set('trackingStatus', true);
    this.trackedRoute = [];

    this.positionSubscription = this.geolocation.watchPosition()
      .subscribe(data => {
        console.log(data);
        this.latitude = data.coords.latitude;
        this.longitude = data.coords.longitude;
        setTimeout(() => {
          this.campaignService.sendRoute({
            campaign_id: this.campaign.id,
            latitude: data['coords']['latitude'],
            longitude: data['coords']['longitude']
          });
        }, 0);
      });
  }

  async stopTracking() {
    this.isTracking = false;
    this.storage.set('trackingStatus', false);
    this.positionSubscription.unsubscribe();
  }

  async takePicture() {
    const options: CameraOptions = {
      quality: 50,
//      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then((imageData) => {
      this.setCurrentLocationAndSendPicture(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  async setCurrentLocationAndSendPicture(imageData) {
    this.loading.present();

    this.geolocation.getCurrentPosition({timeout : 5000}).then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      this.savePictureToDb(imageData);
      this.sendPicture(imageData);
      this.loading.dismiss();
    }).catch(() => {
      this.loading.dismiss();
      this.savePictureToDb(imageData);
      this.errorPopup('Erreur lors d\'envoi de la photo. ' + '\n' +
          'Problème de récupération des donnés GPS ' + '\n' +
          'Merci et d\'activer le GPS et d\'autoriser l\'accès aux données de localisation de votre appareil.');
    });
  }

  async savePictureToDb(imageData) {
    this.database.addImage([
      this.campaign.id,
      imageData,
      this.latitude,
      this.longitude,
      new Date().toJSON().slice(0, 19).replace(/T/g, ' '),
      0
    ]);
  }

  async sendPicture(imageData) {
    const picture = {
      campaign_id: this.campaign.id,
      image: imageData,
      latitude: this.latitude,
      longitude: this.longitude
    };

    this.campaignService.sendPicture(picture).then( () => {
      this.database.updateInsertedImageStatus();
    });
  }

  async errorPopup(error: string) {
    const toast = await this.toastCtrl.create({
      message: error,
      duration: 5000,
      position: 'top',
      closeButtonText: 'OK',
      color: 'danger',
      showCloseButton: true
    });

    await toast.present();
  }

  async gotToImages() {
    this.navCtrl.navigateRoot('/images');
  }
}
