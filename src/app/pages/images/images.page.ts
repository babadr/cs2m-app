import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { DatabaseService, Images } from '../../services/database.service';
import { CampaignService } from '../../services/campaign.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-images',
  templateUrl: './images.page.html',
  styleUrls: ['./images.page.scss'],
})
export class ImagesPage implements OnInit {

  images: Images[] = [];

  constructor(
      private database: DatabaseService,
      private navCtrl: NavController,
      private toastCtrl: ToastController,
      private campaignService: CampaignService,
      private geolocation: Geolocation,
      private loading: LoadingService

  ) { }

  ngOnInit() {
    this.database.getImages().subscribe(images => {
      this.images = images;
    });
  }

  async sendPicture(image_id) {
    const picture = this.images.find(image => image.id == image_id);
    this.setCurrentLocationAndSendPicture(picture);
  }

  async setCurrentLocationAndSendPicture(picture) {
    this.loading.present();

    this.geolocation.getCurrentPosition({timeout : 5000}).then((resp) => {
      picture.latitude = resp.coords.latitude;
      picture.longitude = resp.coords.longitude;
      this.sendPictureData(picture);
      this.loading.dismiss();
    }).catch(() => {
      this.loading.dismiss();
      this.errorPopup('Erreur lors d\'envoi de la photo. ' + '\n' +
          'Problème de récupération des donnés GPS ' + '\n' +
          'Merci et d\'activer le GPS et d\'autoriser l\'accès aux données de localisation de votre appareil.');
    });
  }

  async sendPictureData(picture) {
    this.campaignService.sendPicture(picture).then( () => {
      this.database.updateImageStatus(picture.id);
    });
  }

  async gotToHome() {
    this.navCtrl.navigateRoot('/home');
  }

  async errorPopup(error: string) {
    const toast = await this.toastCtrl.create({
      message: error,
      duration: 5000,
      position: 'top',
      closeButtonText: 'OK',
      color: 'danger',
      showCloseButton: true
    });

    await toast.present();
  }
}
