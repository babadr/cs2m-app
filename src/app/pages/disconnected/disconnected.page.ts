import { Component } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-disconnected',
  templateUrl: './disconnected.page.html',
  styleUrls: ['./disconnected.page.scss'],
})
export class DisconnectedPage {
  constructor( private network: Network, private navCtrl: NavController) {
    const connectSubscription = this.network.onConnect().subscribe(() => {
      this.navCtrl.navigateRoot('/home');
      connectSubscription.unsubscribe();
    });
  }
}
