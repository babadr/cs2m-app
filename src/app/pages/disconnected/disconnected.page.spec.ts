import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisconnectedPage } from './disconnected.page';

describe('ImagesPage', () => {
  let component: DisconnectedPage;
  let fixture: ComponentFixture<DisconnectedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisconnectedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisconnectedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
