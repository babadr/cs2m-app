import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: './pages/login/login.module#LoginPageModule'
  },
  {
    path: 'register',
    loadChildren: './pages/register/register.module#RegisterPageModule'
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadChildren: './pages/home-results/home-results.module#HomeResultsPageModule'
  },
  {
    path: 'images',
    canActivate: [AuthGuard],
    loadChildren: './pages/images/images.module#ImagesPageModule'
  },
  {
    path: 'disconnected',
    loadChildren: './pages/disconnected/disconnected.module#DisconnectedPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
